# Imaging-Basics

## Little Snippets for Image-Processing

* Grayscale_Images (Colormaps and Histogram-Equalization)
* Gauss_and_Laplace (Convolution with Gauss - and Laplace - Kernel)
* Co-Occurence (Co-Occurence of Pixels eg. for classification)
* Color_Histogram (Histogram of Color-Images)
* Color_images (RGB-Separation and two ways of grayscale-conversion)



